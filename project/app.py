from flask import Flask
from flask import jsonify
from flask import request

from config.config import config
from config.sql import SQL

from business.business_logic import *

def create_app(enviroment):
    app = Flask(__name__)
    app.config.from_object(enviroment)
    # init conection sql
    SQL.init()
    return app

enviroment = config['development']
app = create_app(enviroment)

@app.route('/catalog/<name>', methods=['GET'])
def get_users(name):
    return get_catalog({
        'name_catalog': name
    })

@app.route('/solicitud', methods=['POST'])
def post_register_student():
    return register_student(request.get_json(force=True))
    
@app.route('/solicitud/<id>', methods=['PUT'])
def put_register_student(id):
    return update_register_student(id, request.get_json(force=True))
    
@app.route('/solicitud/<id>/estatus', methods=['PATCH'])
def put_accept_student_request(id):
    return accept_student_request(id, request.get_json(force=True))

@app.route('/solicitudes', methods=['GET'])
def get_requests():
    #return get_estudents_by_grimonios()
    return get_requests_estudents()

@app.route('/asignaciones', methods=['GET'])
def request_solicitudes():
    return get_estudents_by_grimonios()

@app.route('/solicitud/<id>', methods=['DELETE'])
def request_delete(id):
    return delete_request_student(id)

if __name__ == '__main__':
    app.run(debug=True, port=4000)